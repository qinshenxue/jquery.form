$.fn.form = function (options) {
    var me = this;

    if (me.length == 0) {
        return me;
    }

    options = $.extend({
        labelSelector: '.control-label',
        formItemSelector: '.form-group',
        errorTipSelector: '.error-tip',
        check: {},
        showErrorTip: function (errorTipMsg, field) {
            field.$errorTip.html(errorTipMsg);
        },
        submit: function () {
            me[0].submit();
        }
    }, options);

    var fields = {};
    me.find('[data-vtype]').each(function () {
        var $formControl = $(this);
        var fieldName = $formControl.attr('name') || $formControl.attr('id');

        if (!fieldName) {
            return true;
        }
        var $formItem = $formControl.closest(options.formItemSelector);
        var vtype = $formControl.data('vtype');
        fields[fieldName] = $.extend({
            name: fieldName,
            label: $formItem.find(options.labelSelector).text(),
            $formControl: $formControl,
            $formItem: $formItem,
            $errorTip: $formItem.find(options.errorTipSelector),
            vtypes: vtype == '' ? [] : vtype.split(',')
        }, $formControl.data());

        var bindEvent = fields[fieldName].vevent || 'blur';

        $formControl.on(bindEvent, function () {
            me.validate(fieldName);
        });

    });

    me.on('submit', function (e) {
        e.preventDefault();
        if (me.isValid()) {
            options.submit(getValues());
        }
    });

    var check = {
        required: function (value) {
            return '' != value;
        },
        requiredMsg: '{label}不能为空'
    };
    $.extend(check, options.check);

    var validate = function (fieldName) {
        var field = fields[fieldName],
            $formControl = field.$formControl,
            $errorTip = field.$errorTip,
            vtypes = field.vtypes,
            value = $formControl.val(),
            result = true;
        if (vtypes.length > 0) {
            for (var i = 0, j = vtypes.length; i < j; i++) {
                var vtype = vtypes[i];
                if (false == check[vtype](value, field)) {
                    var errorTipMsg = formatString(check[vtype + 'Msg'], field);
                    options.showErrorTip(errorTipMsg, field);
                    result = false;
                    break;
                } else {
                    options.showErrorTip('', field);
                }
            }
        }
        return result;
    };

    var isValid = function () {
        var result = true;
        for (var fieldName in fields) {
            if (false == validate(fieldName)) {
                result = false;
            }
        }
        return result;
    };

    var getValues = function () {
        var params = {};
        for (var i in fields) {
            params[i] = fields[i].$formControl.val();
        }
        return params;
    };

    var formatString = function (format, data) {
        return format.replace(/\{(\w+)\}/g, function (match, key) {
            return data[key] || '';
        });
    };

    me.fields = fields;
    me.getValues = getValues;
    me.validate = validate;
    me.isValid = isValid;
    return me;
};