#jquery.form

## 演示地址
[http://www.qinshenxue.com/demo/form/demo.html](http://www.qinshenxue.com/demo/form/demo.html)

## 使用方法


### 输入框的name（或id）属性是必须的，否则不会被纳入验证范围，如果同时存在name和id，以name为输入框标识
标识取[name]
```
<input type="text" name="uid" >
<input type="text" name="uid" id="username">
```
标识取[id]
```
<input type="text" id="uid">
```

### 对于要验证的输入框必须加上data-vtype后才会被提取
如果data-vtype不赋值，同样会被提取（给予验证结果为true），输入框的值会被getValues方法获取到。
```
<input type="text" name="uid"  data-vtype>
<input type="text" name="uid"  data-vtype="">
```
### data-vtype的值即要验证的类型
多个验证类型时，逗号隔开，验证顺序为依次验证，一旦一个验证不通过，后面的就不再验证
. required：必填
. email：邮箱验证
. 自己可随便定义
```
<input type="text" name="uid" class="form-control" placeholder="请输入用户名" data-vtype="required">
```
```
<input type="text" name="email" class="form-control" placeholder="请输入邮箱地址" data-vtype="required,email">
```
### 自定义验证类型
自定义放在check配置下，相应的要配置错误提示消息，格式为验证方法名+Msg。{label}为字段的名字，也可以充分自定义错误提示消息格式
要注意的自定义验证类型方法的返回值，如果验证通过就返回true，虽然读起来感觉在说废话，但是在写代码的时候会有点绕。比如必填的返回就是`return value!=''`
1. 传入参数value为输入框的值
2. 传入参数field值格式如下
field还包括输入框添加的data-*属性及值
```
{
    "name": "name属性值或id属性值（字符串）",
    "label": "label名字（字符串）",
    "$formControl": "输入框标签（jquery对象）",
    "$formItem": "form组成单元（jquery对象）",
    "$errorTip": "错误提示标签（jquery对象）",
    "vtypes": "vtype逗号分隔的数组（数组）",
    "vtype":"required",
    "maxlength": "10",
    "range":"6,10",
    "...":"..."
}
```
自定义验证类型
```
<script>
    $('#demo_form').form({
        check: {
            exist: function (value, field) {
                return true;
            },
            existMsg: '{label}已存在'
        }
    });
</script>
```
自定义错误提示消息格式
```
<input type="text" name="uid" class="form-control" placeholder="请输入用户名" data-vtype="required,maxlength" data-maxlength="10">
<script>
    $('#demo_form').form({
        check: {
            maxlength: function (value, field) {
                return value.length>field.maxlength
            },
            maxlengthMsg: '{label}不能超过{maxlength}个字符',
            range: function (v, field) {
                var rangeArr = field.range.split(',');
                field.rangeMin = rangeArr[0];
                field.rangeMax = rangeArr[1];
                return v.length >= rangeArr[0] && v.length <= rangeArr[1];
            },
            rangeMsg: '{label}长度在{rangeMin}~{rangeMax}个字符之间',
        }
    });
</script>
```

### 自定义提交方法
默认为form提交，如果想改为ajax提交可以配置如下。
参数values为form表单的参与验证的（加了data-vtype）的键值对
```
<script>
    $('#demo_form').form({
        check: {
            submit:function(values){
            //ajax代码
            }
        }
    });
</script>
```

### 单独验证某项
可以使用validate方法，比如发送短信的场景
传入参数为输入框的标识
```
<input type="text" name="cellphone" id="phone" class="form-control" placeholder="请输入用户名" data-vtype="required,cellphone">
<script>
    var form=$('#demo_form').form();
    if(form.validate('cellphone')){
        // 发送短信代码
    }
</script>
```